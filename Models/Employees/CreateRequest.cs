namespace WebApi.Models.Employees;

using System.ComponentModel.DataAnnotations;
using WebApi.Entities;

public class CreateRequest
{
    [Required]
    public string FullName { get; set; }

    [Required]
    public string PhoneNumber { get; set; }

}