namespace WebApi.Models.Employees;

using System.ComponentModel.DataAnnotations;
using WebApi.Entities;

public class UpdateRequest
{
    public string FullName { get; set; }
    public string PhoneNumber { get; set; }
}