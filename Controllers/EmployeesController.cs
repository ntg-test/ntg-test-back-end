﻿namespace WebApi.Controllers;

using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebApi.Services;

[ApiController]
[Route("[controller]")]
public class EmployeesController : ControllerBase
{
    private IEmployeeService _employeeService;
    private IMapper _mapper;

    public EmployeesController(
        IEmployeeService employeeService,
        IMapper mapper)
    {
        _employeeService = employeeService;
        _mapper = mapper;
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        var employees = _employeeService.GetAll();
        return Ok(employees);
    }

    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
        var employee = _employeeService.GetById(id);
        return Ok(employee);
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        _employeeService.Delete(id);
        return Ok(new { message = "User deleted" });
    }
}