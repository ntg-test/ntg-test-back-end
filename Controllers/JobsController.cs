﻿namespace WebApi.Controllers;

using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebApi.Services;

[ApiController]
[Route("[controller]")]
public class JobsController : ControllerBase
{
    private IJobService _jobService;
    private IMapper _mapper;

    public JobsController(
        IJobService jobService,
        IMapper mapper)
    {
        _jobService = jobService;
        _mapper = mapper;
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        var jobs = _jobService.GetAll();
        return Ok(jobs);
    }

    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
        var job = _jobService.GetById(id);
        return Ok(job);
    }


    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        _jobService.Delete(id);
        return Ok(new { message = "Job deleted" });
    }
}