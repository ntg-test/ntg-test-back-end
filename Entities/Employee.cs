namespace WebApi.Entities;

public class Employee
{
    public int Id { get; set; }
    public string FullName { get; set; }
    public string PhoneNumber { get; set; }
    
    public int JobID { get; set; }
    public Job Job { get; set; }



}