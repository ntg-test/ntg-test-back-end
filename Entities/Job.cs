namespace WebApi.Entities;

public class Job
{
    public int Id { get; set; }
    public string Description { get; set; }
    public ICollection<Employee> employees { get; set; }

}