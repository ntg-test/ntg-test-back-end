namespace WebApi.Services;

using AutoMapper;
using BCrypt.Net;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Employees;

public interface IEmployeeService
{
    IEnumerable<Employee> GetAll();
    Employee GetById(int id);
    void Delete(int id);
}

public class EmployeeService : IEmployeeService
{
    private DataContext _context;
    private readonly IMapper _mapper;

    public EmployeeService(
        DataContext context,
        IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public IEnumerable<Employee> GetAll()
    {
        return _context.Employees;
    }

    public Employee GetById(int id)
    {
        return getEmployee(id);
    }

    public void Delete(int id)
    {
        var employee = getEmployee(id);
        _context.Employees.Remove(employee);
        _context.SaveChanges();
    }

    // helper methods

    private Employee getEmployee(int id)
    {
        var employee = _context.Employees.Find(id);
        if (employee == null) throw new KeyNotFoundException("Employee not found");
        return employee;
    }
}