namespace WebApi.Services;

using AutoMapper;
using BCrypt.Net;
using WebApi.Entities;
using WebApi.Helpers;

public interface IJobService
{
    IEnumerable<Job> GetAll();
    Job GetById(int id);
    void Delete(int id);
}

public class JobService : IJobService
{
    private DataContext _context;
    private readonly IMapper _mapper;

    public JobService(
        DataContext context,
        IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public IEnumerable<Job> GetAll()
    {
        return _context.Jobs;
    }

    public Job GetById(int id)
    {
        return getJob(id);
    }

    public void Delete(int id)
    {
        var job = getJob(id);
        _context.Jobs.Remove(job);
        _context.SaveChanges();
    }

    // helper methods

    private Job getJob(int id)
    {
        var job = _context.Jobs.Find(id);
        if (job == null) throw new KeyNotFoundException("Job not found");
        return job;
    }
}